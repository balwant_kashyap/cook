=== Spice Cookies ===

Contributors: spicethemes
Tags: cookie, cookies, cookie notification
Requires at least: 5.3
Requires PHP: 5.2
Tested up to: 5.8
Stable tag: 0.1
License: GPLv2 or later
License URI: http://www.gnu.org/licenses/gpl-2.0.html

== Description ==

This plugin allows to create easy display notification message that informs site user to use cookies for your website.

== Changelog ==

@Version 0.1
* Initial Release.

======= External Resources =======

Js Cookie:
Copyright: Copyright (c) 2018 Copyright 2018 Klaus Hartl, Fagner Brack, GitHub Contributors
License: MIT License
Source: https://github.com/carhartl/jquery-cookie