<?php
/*
* Plugin Name:			Spice Cookies
* Plugin URI:  			
* Description: 			This plugin allows to create easy display notification message that informs site user to use cookies for your website.
* Version:     			0.1
* Requires at least: 	5.3
* Requires PHP: 		5.2
* Tested up to: 		5.8
* Author:      			Spicethemes
* Author URI:  			https://spicethemes.com
* License: 				  GPLv2 or later
* License URI: 			https://www.gnu.org/licenses/gpl-2.0.html
* Text Domain: 			spice-cookies
* Domain Path:  		/languages
*/

// Exit if accessed directly
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

// define the constant for the URL
define( 'SPICE_COOKIES_PLUGIN_URL', plugin_dir_url( __FILE__ ) );
define( 'SPICE_COOKIES_PLUGIN_DIR', plugin_dir_path( __FILE__ ) );

add_action( 'init', 'spice_cookies_load_textdomain' );
/**
 * Load plugin textdomain.
 */
function spice_cookies_load_textdomain() {
  load_plugin_textdomain( 'spice-cookies', false, plugin_dir_url(__FILE__). 'languages' );

}


//Define the function for the plugin activation
function spice_cookies_activation() {

	require_once SPICE_COOKIES_PLUGIN_DIR . 'inc/customizer/customizer-class.php';

	//Font file
	require_once SPICE_COOKIES_PLUGIN_DIR . 'inc/spice-cookies-fonts.php';

	//Cookies
	require_once SPICE_COOKIES_PLUGIN_DIR . 'inc/cookie-app.php';

}
add_action('plugins_loaded', 'spice_cookies_activation');