<?php
function spice_cookies_callback()
{
if (! isset($_COOKIE["spice_cookies"])) :?>
<input type="hidden" id="spice_cook_exp" value="<?php echo get_theme_mod('spice_cookies_expire',10);?>">  
<div id="spice-cookie-section" class="spice-cookie-area">
	<div id="spice-cookie-content">
		<p class="spice-cookie-para"><?php echo get_theme_mod('spice_cookies_content',esc_html__( 'This website uses cookies to ensure you get the best experience on our website.', 'spice-cookies' ));?></p>
		<a class="spice-cookie-btn" href="#"><?php echo get_theme_mod('spice_cookies_button',esc_html__( 'Accept', 'spice-cookies' ));?></a>
	</div>
</div>
<?php endif;?>
<style type="text/css">
#spice-cookie-section #spice-cookie-content .spice-cookie-para
{
	font-family: <?php echo esc_attr(get_theme_mod('spice_cookies_fontfamily','Poppins'));?>;
	font-size:   <?php echo esc_attr(get_theme_mod('spice_cookies_fontsize',16));?>px;
	font-weight: <?php echo esc_attr(get_theme_mod('spice_cookies_fontweight',400));?>;
	font-style:  <?php echo esc_attr(get_theme_mod('spice_cookies_fontstyle','normal'));?>;
	text-transform: <?php echo esc_attr(get_theme_mod('spice_cookies_transform','default'));?>;
}
#spice-cookie-section #spice-cookie-content .spice-cookie-btn
{
	font-family: <?php echo esc_attr(get_theme_mod('spice_btn_fontfamily','Poppins'));?>;
	font-size:   <?php echo esc_attr(get_theme_mod('spice_btn_fontsize',16));?>px;
	font-weight: <?php echo esc_attr(get_theme_mod('spice_btn_fontweight',400));?>;
	font-style:  <?php echo esc_attr(get_theme_mod('spice_btn_fontstyle','normal'));?>;
	text-transform: <?php echo esc_attr(get_theme_mod('spice_btn_transform','default'));?>;
}	
</style>
<?php if(get_theme_mod('enable_cookies_clr',false) == true ):?>
<style type="text/css">
body #spice-cookie-section	{ background-color: <?php echo esc_attr(get_theme_mod('spice_cookies_bg_color','#000000'));?>;}
body #spice-cookie-content .spice-cookie-para{color:<?php echo esc_attr(get_theme_mod('spice_cookies_text_color','#858585'));?>;}
body .spice-cookie-btn{ background-color: <?php echo esc_attr(get_theme_mod('spice_cookies_btn_bg_color','#ff6f61'));?>;} 
body .spice-cookie-btn{ color: <?php echo esc_attr(get_theme_mod('spice_cookies_btn_color','#ffffff'));?>;} 
body .spice-cookie-btn:hover{ background-color: <?php echo esc_attr(get_theme_mod('spice_cookies_btn_bg_colorr','#ff6f61'));?>;}
body .spice-cookie-btn:hover{color: <?php echo esc_attr(get_theme_mod('spice_cookies_btn_colorr','#ffffff'));?>;} 
</style>
<?php endif;
}

add_action('spice_cookies','spice_cookies_callback'); ?>	