<?php
// Exit if accessed directly
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

if ( ! class_exists( 'Spice_Cookies_Customizer' ) ) :

	/**
	 * Spice Cookies Customizer class
	*/
	class Spice_Cookies_Customizer {

		/**
		 * Setup class
		*/
		public function __construct() 
		{
			add_action( 'customize_register', array( $this, 'spice_cookies_custom_controls' ) );
			add_action( 'after_setup_theme', array( $this, 'spice_cookies_register_options' ) );
			add_action( 'wp_enqueue_scripts', array( $this,'spice_cookies_load_script'));
			add_action( 'admin_enqueue_scripts', array( $this,'spice_cookies_load_admin_script'));
		}

		/**
		 * Adds custom controls
		*/
		public function spice_cookies_custom_controls( $wp_customize ) 
		{
			require_once ( SPICE_COOKIES_PLUGIN_DIR . '/inc/customizer/controls/toggle/class-toggle-control.php' );
			require_once ( SPICE_COOKIES_PLUGIN_DIR . '/inc/customizer/controls/color/color-control.php' );
			$wp_customize->register_control_type('Spice_Cookie_Toggle_Control');
		}

		/**
		 * Adds customizer options
		*/
		public function spice_cookies_register_options() 
		{
			require_once ( SPICE_COOKIES_PLUGIN_DIR . '/inc/customizer/sanitization.php' );
			require_once ( SPICE_COOKIES_PLUGIN_DIR . '/inc/customizer/customizer.php' );
		}

		/**
		 * Load Js
		*/
		public function spice_cookies_load_script()
	     {
	     	 //wp_enqueue_script('spice-cookies-jquery', 'https://code.jquery.com/jquery-1.12.0.min.js');	
		  	  wp_enqueue_script('spice-cookies-lib', SPICE_COOKIES_PLUGIN_URL . 'js/cookies.js', array('jquery'));
		  	  wp_enqueue_script('spice-cookies-custom', SPICE_COOKIES_PLUGIN_URL . 'js/custom.js', array('jquery'));
		      wp_enqueue_style('spice-cookies-custom',  SPICE_COOKIES_PLUGIN_URL. 'css/custom.css');
	  	
	     }

	     public function spice_cookies_load_admin_script()
	     {
	     	wp_enqueue_style('spice-cookies-admin',  SPICE_COOKIES_PLUGIN_URL. 'css/admin.css');

	     }

	}

endif;

new Spice_Cookies_Customizer();	