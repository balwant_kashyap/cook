<?php

if ( ! defined( 'ABSPATH' ) ) {
    exit;
}

// callback function for the footer background color
function spice_cookies_color_callback($control) {
    if (false == $control->manager->get_setting('enable_cookies_clr')->value()) {
        return false;
    } else {
        return true;
    }
}

/**
 * Select choices sanitization callback
*/
function spice_cookies_sanitize_select($input, $setting) {

    //input must be a slug: lowercase alphanumeric characters, dashes and underscores are allowed only
    $input = sanitize_key($input);

    //get the list of possible radio box options 
    $choices = $setting->manager->get_control($setting->id)->choices;

    //return input if valid or return default option
    return ( array_key_exists($input, $choices) ? $input : $setting->default );
}

/**
 * Checkbox sanitization callback
*/
function spice_cookies_sanitize_checkbox($checked) {

    // Boolean check.
    return ( ( isset($checked) && true == $checked ) ? true : false );

}